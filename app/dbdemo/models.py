from django.db import models


class DbDemo(models.Model):
    field1 = models.CharField(max_length=200)
    field2 = models.CharField(max_length=200, default='field2')
    field3 = models.CharField(max_length=200, default='field3')
