docker run -d \
    --name postgres \
    -e POSTGRES_PASSWORD=hello_django \
    -e POSTGRES_DB=hello_django \
    -e POSTGRES_USER=hello_django \
    -p 5432:5432 \
    postgres