kind create cluster --name adx

helm upgrade -i postgresql bitnami/postgresql --set postgresqlUsername=hello_django --set postgresqlPassword="hello_django" --set postgresqlDatabase="hello_django"

docker build -t vdksystem/django-demo:v0.0.1-alpha.1 .

kind load docker-image vdksystem/django-demo:v0.0.1-alpha.1 --name adx

helm upgrade -i backend backend

k exec -it backend-7456dfcb5b-877gq -- sh

python manage.py createsuperuser --username=dkuleshov --email=dkuleshov@adxnet.com

kpf svc/backend 8000:80

http://localhost:8000/admin/dbdemo/dbdemo/

make changes to model

python manage.py makemigrations dbdemo

docker build -t vdksystem/django-demo:v0.0.1-alpha.2 .

kind load docker-image vdksystem/django-demo:v0.0.1-alpha.2 --name adx

change values.yaml with new image tag for image AND db-image

helm upgrade -i backend backend

kpf svc/backend 8000:80

http://localhost:8000/admin/dbdemo/dbdemo/